#!/bin/bash

# Função para gerar um número aleatório entre 2 e 13
gerar_numero_aleatorio() {
  echo $((RANDOM % 12 + 2))
}

# Primeiro Arquivo

# Verifica se o arquivo já existe
  if [ -e "$nome_arquivo" ]; then
    echo "O arquivo $nome_arquivo já existe. Gerando outro nome."
  else
    touch "$nome_arquivo"
    echo "Arquivo criado: $nome_arquivo"
  fi

numero=$(gerar_numero_aleatorio)
nome_arquivo="${numero}.txt"
if [ -e "$nome_arquivo" ]; then
    echo "O arquivo $nome_arquivo já existe. Gerando outro nome."
  else
    touch "$nome_arquivo"
    echo "Arquivo criado: $nome_arquivo"
  fi
# Segundo Arquivo
numero=$(gerar_numero_aleatorio)
nome_arquivo="${numero}.txt"
if [ -e "$nome_arquivo" ]; then
    echo "O arquivo $nome_arquivo já existe. Gerando outro nome."
  else
    touch "$nome_arquivo"
    echo "Arquivo criado: $nome_arquivo"
  fi
# Terceiro Arquivo
numero=$(gerar_numero_aleatorio)
nome_arquivo="${numero}.txt"
if [ -e "$nome_arquivo" ]; then
    echo "O arquivo $nome_arquivo já existe. Gerando outro nome."
  else
    touch "$nome_arquivo"
    echo "Arquivo criado: $nome_arquivo"
  fi
