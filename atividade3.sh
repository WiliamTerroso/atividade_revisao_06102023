 #!/bin/bash

# Inicializa contadores
existentes=0
nao_existentes=0

# Verifica a existência de cada arquivo individualmente
if [ -e "$1" ]; then
  existentes=$((existentes + 1))
else
  nao_existentes=$((nao_existentes + 1))
fi

if [ -e "$2" ]; then
  existentes=$((existentes + 1))
else
  nao_existentes=$((nao_existentes + 1))
fi

if [ -e "$3" ]; then
  existentes=$((existentes + 1))
else
  nao_existentes=$((nao_existentes + 1))
fi

if [ -e "$4" ]; then
  existentes=$((existentes + 1))
else
  nao_existentes=$((nao_existentes + 1))
fi

if [ -e "$5" ]; then
  existentes=$((existentes + 1))
else
  nao_existentes=$((nao_existentes + 1))
fi

# Imprime os resultados
echo "Quantidade de arquivos existentes: $existentes"
echo "Quantidade de arquivos não existentes: $nao_existentes"

