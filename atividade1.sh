#!/bin/bash
#
# Função para gerar um número aleatório entre 2 e 13
gerar_numero_aleatorio() {
  echo $((RANDOM % 12 + 2))
 }

# Primeiro Arquivo
numero=$(gerar_numero_aleatorio)
nome_arquivo="${numero}.txt"
touch "$nome_arquivo"
echo "Arquivo criado: $nome_arquivo"
#  # Segundo Arquivo
numero=$(gerar_numero_aleatorio)
nome_arquivo="${numero}.txt"
touch "$nome_arquivo"
echo "Arquivo criado: $nome_arquivo"
# Terceiro Arquivo
numero=$(gerar_numero_aleatorio)
nome_arquivo="${numero}.txt"
touch "$nome_arquivo"
echo "Arquivo criado: $nome_arquivo"
