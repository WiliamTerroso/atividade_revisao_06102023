 #!/bin/bash

arquivo1=$1
arquivo2=$2
test -e $arquivo1 && echo OK || echo Fail
test -e $arquivo2 && echo OK || echo Fail

# Calculando numeros de Linha dos Arquivos
linhas_arquivo1=$(wc -l < "$arquivo1")
linhas_arquivo2=$(wc -l < "$arquivo2")

# Comparação de Arquivos
test $linhas_arquivo1 -gt $linhas_arquivo2 && echo "O arquivo $arquivo1 possui mais linhas do que o arquivo $arquivo2 ." && linhas_arquivo1=$(wc -l < "$arquivo1") 
test $linhas_arquivo1 -lt $linhas_arquivo2 && echo "O arquivo $arquivo2 possui menos linhas do que o arquivo $arquivo1 ." && linhas_arquivo2=$(wc -l < "$arquivo2") 
